# lateX 

Soit $x\in\displaystyle\int_{u=1}^{y=2}dx(\frac{3}{2} x^2)$ tel que $ x^2 = 5$ et sans espace : $x^2=5$

\begin{equation}
3x + 2 = 5 \Leftrightarrow x = 1 
\end{equation}

$$ - \sqrt{3} + 2 = 18\times x^2$$ 

\begin{eqnarray}
mx - 8 &=& \frac{7}{3} \\
&=& 8 \displaystyle \lim_{x= 2} f(x) 
\end{eqnarray}