>pip install mkdocs

creation d'un repertoire "docs" ayant tous les fichiers du site

puis creation d'un fichier de configuration du site (pas dans "docs", mais dans le rep du site)
> mkdocs.yml 

pour créer un petit serveur virtuel qui va lire le mkdocs.yml
> mkdocs serve 


pour installer les options diverses (colorations, makeup, extensions ..)
> pip install mkdocs-material


pour construire le site avec les htmls
> mkdocs build


